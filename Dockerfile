# Build with:
# 	docker build -t cswl/xampp .

FROM ubuntu:16.04

ENV TERM=xterm

# Install curl and net-stats for XAMPP 
RUN apt-get update && \
 apt install -yq curl net-tools psmisc

# Download the installer (5.6.21)
RUN curl -o xampp-linux-installer.run "https://downloadsapachefriends.global.ssl.fastly.net/xampp-files/5.6.21/xampp-linux-x64-5.6.21-0-installer.run?from_af=true"
RUN chmod +x xampp-linux-installer.run
RUN bash -c './xampp-linux-installer.run'

# Run the installer in Unattended mode
# Currently there is a bug which doesnt show progress
#RUN chmod 755 xampp-linux-installer.run
#RUN ./xampp-linux-installer.run --mode unattended --unattendedmodeui  minimal 

# Delete the installer file after install
RUN rm ./xampp-linux-installer.run

# Enable XAMPP web interface(remove security checks)
RUN /opt/lampp/bin/perl -pi -e s'/Require local/Require all granted/g' /opt/lampp/etc/extra/httpd-xampp.conf

# Enable includes of several configuration files
RUN mkdir /opt/lampp/apache2/conf.d && \
echo "IncludeOptional /opt/lampp/apache2/conf.d/*.conf" >> /opt/lampp/etc/httpd.conf

# Create a /www folder and a symbolic link to it in /opt/lampp/htdocs. 
# It'll be accessible via http://localhost:[port]/www/
# This is convenient because it doesn't interfere with xampp, phpmyadmin or other tools in /opt/lampp/htdocs
RUN mkdir /www
RUN ln -s /www /opt/lampp/htdocs/

# Link to /usr/bin for easier starting
RUN ln -sf /opt/lampp/lampp /usr/bin/lampp

# Add xampp binaires to .bashrc
RUN echo "export PATH=\$PATH:/opt/lampp/bin/" >> /root/.bashrc
RUN echo "export TERM=xterm" >> /root/.bashrc

# Add inoncube ext

#RUN curl -o ioncube.tar.gz http://downloads3.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.tar.gz \
#    && tar -xvvzf ioncube.tar.gz \
#    && mv ioncube/ioncube_loader_lin_5.6.so `php-config --extension-dir` \
#    && rm -Rf ioncube.tar.gz ioncube \
#    && docker-php-ext-enable ioncube_loader_lin_5.6

COPY /download/ioncube/ioncube_loader_lin_5.6.so /opt/lampp/lib/php/extensions/no-debug-non-zts-20131226/
RUN sed -i '1i zend_extension = /opt/lampp/lib/php/extensions/no-debug-non-zts-20131226/ioncube_loader_lin_5.6.so' /opt/lampp/etc/php.ini

EXPOSE 80 443 3306

ADD init.sh /usr/local/bin/init.sh
RUN chmod 777 /usr/local/bin/init.sh

# Start the init script
ENTRYPOINT ["/usr/local/bin/init.sh"]
